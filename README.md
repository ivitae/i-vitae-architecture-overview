Client Web Interface
--
* content management and CRM tooling provided by HubSpot
* integrates with existing back end services

Client Side Application
--
* Android and iOS native applications created using React Native
* Single codebase for both platforms
* Deployed through App Store and Google Play Store manually, after round of manual testing

Backend Dashboard Application
--
* Dashboard Application created using Ruby on Rails
* Served to the web using Elastic Beanstalk service on Amazon Web Services
* Continuously deployed and tested using Circle CI
* Automated test suite run on every commit, before automated deployment
* Integrates with 3rd party APIs
  * Hubspot (CRM and non-sensitive user information)
  * chino.io (Backend as a Service for sensitive and/or medical information)
  * Stripe (Payments as a service)
  * Amazon RDS relational database service
  * Others� tbd

Chat Microservice
--
* realtime app/backend communication server
* Application created using Node.js
* Served to the web using Elastic Beanstalk service on Amazon Web Services
* Continuously deployed and tested using Circle CI
* Automated test suite run on every commit, before automated deployment
* Possible 3rd party integrations (if needed, possibly out of scope for MVP)
  * Api.ai (or similar) for Natural language processing

  
Payment Microservice
--
* No credit card information stored or passed to our backend, handles Stripe Payment Tokens only
* Served to the web using Lambda and API Gateway services on Amazon Web Services
* Application created using Node.js

Miscellanious
--
* All source code to be hosted privately on bitbucket.org
* Continuous integration and deployment to be handled by CircleCI where appropriate
* Logging to be handled by Loggly (or similar) where appropriate
* Analytics to be handled by Google Analytics (or similar) as appropriate